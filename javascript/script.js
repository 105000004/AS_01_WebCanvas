var canvas;
var context;


window.onload = function() {
  canvas = document.getElementById("drawingCanvas");
  context = canvas.getContext("2d");
  canvas.onmousedown = startDrawing;
  canvas.onmouseup = stopDrawing;
  canvas.onmouseout = stopDrawing;
    w = canvas.width;
    h = canvas.height;

    canvas.addEventListener("mousemove", function (e) {
        findxy('move', e)
    }, false);
    canvas.addEventListener("mousedown", function (e) {
        findxy('down', e)
    }, false);
    canvas.addEventListener("mouseup", function (e) {
        findxy('up', e)
    }, false);
    canvas.addEventListener("mouseout", function (e) {
        findxy('out', e)
    }, false);
}

var isDrawing = false;
var start_x;
var start_y;
var end_x;
var end_y;
var drawcircle = false;
var flag = false;
var drawline = false;
var drawsquare = false;
var drawtriangle = false;
var fill = false;
var eraser = false;
var eraserSize = number;
var inputtext = false;

/*enterText*/
function inputText(){
    //window.alert("eraser");
    inputtext = true;
    flag = true;
    eraser = false;
    drawtriangle = false;
    drawline = false;
    drawcircle = false;
    drawsquare = false;
    document.getElementById("drawingCanvas").style = "cursor:grab;";
}

/*eraser*/
function Eraser(){
    //window.alert("eraser");
    eraser = true;
    inputtext = false;
    drawtriangle = false;
    flag = true;
    drawline = false;
    drawcircle = false;
    drawsquare = false;
    drawLine_do();
    document.getElementById("drawingCanvas").style = "cursor:cell;";
}

/*Draw a circle*/
function drawCircle(){
    drawtriangle = false;
    flag = true;
    inputtext = false;
    drawline = false;
    drawcircle = true;
    eraser = false;
    drawsquare = false;
    fill = false;
    document.getElementById("drawingCanvas").style = "cursor:move;";
}

function saveFile() {
    dataURL = canvas.toDataURL();
    document.getElementById("canvasimg").style.display = "inline";
}

/*Draw a Line*/
function drawLine() {
    drawtriangle = false;
    drawline = true;
    inputtext = false;
    drawsquare = false;
    drawcircle = false;
    flag = false;
    eraser = false;
    
    drawLine_do();
    fill = false;
    
    document.getElementById("drawingCanvas").style = "cursor:crosshair;";
}

/*Draw a square*/
function drawSquare() {
    drawtriangle = false;
    drawline = false;
    inputtext = false;
    flag = true;
    eraser = false;
    drawcircle = false;
    drawsquare = true;
    fill = false;
    document.getElementById("drawingCanvas").style = "cursor:nw-resize;";
}

/*Draw a triangle*/
function drawTriangle(){
    drawtriangle = true;
    drawline = false;
    inputtext = false;
    drawsquare = false;
    drawcircle = false;
    eraser = false;
    flag = true;
    fill = false;
    document.getElementById("drawingCanvas").style = "cursor:ne-resize;";
}

function Fill(){
    fill = true;
    if(drawtriangle == true){
        document.getElementById("drawingCanvas").style = "cursor:ne-resize;";
    }else if(drawline == true){
        document.getElementById("drawingCanvas").style = "cursor:crosshair;";
    }else if(drawsquare == true){
        document.getElementById("drawingCanvas").style = "cursor:nw-resize;";
    }else if(drawcircle == true){
        document.getElementById("drawingCanvas").style = "cursor:move;";
    }
    
}

/* 开始画图*/
function startDrawing(e) {
    //window.alert("eraser");
    //draw circle,square, get x,y
    if((drawcircle == true || drawsquare == true || drawtriangle == true || inputtext == true) && flag == true){
        //window.alert("eraser");
        start_x = e.pageX - canvas.offsetLeft;
        start_y = e.pageY - canvas.offsetTop;
        flag = false;
    }
  isDrawing = true;
  context.beginPath();
  context.moveTo(e.pageX - canvas.offsetLeft, e.pageY - canvas.offsetTop);
}

/* 停止画图*/
function stopDrawing(e) {
    //input text
    if(inputtext == true && isDrawing == true){
        end_x = e.pageX - canvas.offsetLeft;
        end_y = e.pageY - canvas.offsetTop;
        var t_text = document.getElementById("text_text").value;
        var t_size = document.getElementById("text_size").value;
        var t_style = document.querySelector('input[name="text_style"]:checked').value;
        //-------------------------------------
        var fontArgs = context.font.split(' ');
        var newSize = t_size + 'px';
        context.font = newSize + ' ' + t_style;
        //--------------------------------------
        context.fillText(t_text,(start_x+end_x)/2,(start_y+end_y)/2);
        flag = true;
    }
    if(drawline == true && isDrawing == true){
        if(fill == true){
            context.fill();
        }
    }
    
    //draw circle
    if(drawcircle == true && isDrawing == true){
        end_x = e.pageX - canvas.offsetLeft;
        end_y = e.pageY - canvas.offsetTop;
        context.beginPath();
        context.arc(start_x,start_y,(Math.sqrt((start_x-end_x)*(start_x-end_x)+(start_y-end_y)*(start_y-end_y))),0,2*Math.PI);
        if(fill == true){
            context.fill();
        }
        context.stroke();
        flag = true;
    }
    //draw square
    if(drawsquare == true && isDrawing == true){
        end_x = e.pageX - canvas.offsetLeft;
        end_y = e.pageY - canvas.offsetTop;
        context.beginPath();
        context.rect(start_x,start_y,Math.abs(end_x-start_x),Math.abs(end_y-start_y));
        if(fill == true){
            context.fill();
        }
        context.stroke();
        flag = true;
    }
    //draw a triangle
    if(drawtriangle == true && isDrawing == true){
        end_x = e.pageX - canvas.offsetLeft;
        end_y = e.pageY - canvas.offsetTop;
        context.moveTo(start_x,start_y);
        context.lineTo(end_x,end_y);
        context.lineTo(end_x-2*(end_x-start_x),end_y);
        context.lineTo(start_x,start_y);
        context.lineTo(end_x,end_y);
        if(fill == true){
            context.fill();
        }
        context.stroke();
        flag = true;
    }
    isDrawing = false;
}

function drawLine_do(e) {
    colorValue = document.getElementById("color_selecter").value;
    if(drawline == true || eraser == true){
        //window.alert("drawLine");
        canvas.onmousemove = drawLine_do;

    }
    //eraser
    if (eraser == true && isDrawing == true){
        var x = e.pageX - canvas.offsetLeft;
        var y = e.pageY - canvas.offsetTop;
        //window.alert("!!");
        context.lineTo(x, y);
        context.stroke(); 
        context.strokeStyle = "#FFFFFF";
        context.fillStyle = "#FFFFFF";
    }
    //line
    if (isDrawing == true && drawline == true) {
        var x = e.pageX - canvas.offsetLeft;
        var y = e.pageY - canvas.offsetTop;
        context.lineTo(x, y);
        context.stroke();  
        context.strokeStyle = colorValue;
    context.fillStyle = colorValue;
    }
}

function changeColor(colorValue){
    context.strokeStyle = colorValue;
    context.fillStyle = colorValue;
}

/*thickness*/
function changeThickness(thickness){
    if(thickness == -1){
        var x = document.getElementById("thickness").value;
        context.lineWidth = x;
    }
    else{
        context.lineWidth = thickness;
    }
}


function saveCanvas() { 
    var canvas = document.getElementById('drawingCanvas');
    function downloadCanvas(link, canvasId, filename) {
        link.href = document.getElementById(canvasId).toDataURL();
        link.download = filename;
    }
    document.getElementById('download').addEventListener('click', function() {
        downloadCanvas(this, 'drawingCanvas', 'download.png');
    }, false);
}
function changeCursor() {
    alert(document.getElementById("drawingCancas").style.cursor);
}

